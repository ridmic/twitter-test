# Twitter API Test

This is a simple project to test out access to the Twitter APIs. The code allows for the searching of the latest tweets 
for a specified hashtag and returns a summary of these.

The code shows the separation of responsibilities between the Twitter API Access class and the routes/views.

## Dependencies  
* This project was written for PHP 7.2+ but should validate against other versions. You may need to remove the type-hints
if you wish to run this under PHP Ver 5.x or lower.
* You will need to install the php curl package. Depending on your operating system, this can be done with a command such
as : **sudo apt-get install php7.2-curl**

## Third party modules
This project uses a couple of commonly used open-source modules that are ideal for creating PoCs:

* The routing is done via the ['Dispatch.php'](https://github.com/noodlehaus/dispatch) module
* The Twitter API Wrapper is done via the ['TwitterAPIExchange.php](https://github.com/J7mbo/twitter-api-php) module.

## Twitter Account and Credentials
In order to run this application you will need an active Twitter account and to have created a Twitter App. If you have
not done this before, see ['this link'](http://docs.inboundnow.com/guide/create-twitter-application/) for guidance.

Once you have a valid Twitter account and application, you will need to retrieve your access tokens and add then to 
the config.php file. This file is ignored in git so that we do not push our credentials up with the source code. There is a file
called config-sample.php that shows the format for this file. This sample file should be copied to config.php and then 
you should fill that in with your own credentials. 


## Entry Points
 
There are three main entry points to test out the code:

1. Command Line

2. HTML Pages

3. Simple RESTful API

## Command Line
To run from the command line, you can just run the following directly (if you have a php intepreter):

**twitterApp.php**:  This is set up to just search for the latest 10 items that contain the hashtag #worldcup
 
## HTML Pages
The HTML pages give a simple level of interaction to test out the project. You can start the process listening locally
by opening up a terminal, moving to the **twitter-test** folder and then using the command: 

**php -S localhost:8080 twitterService.php**. 

Changes can be made to the code whilst this process is running and they will be reflected in the next invocation.

There are a few routes active for the HTML:

1. localhost:8080/ -> The home/welcome page. 
2. localhost:8080/latest/:tag -> Shows a summary of the latest 10 tweets containing the hashtag ':tag'

## Simple API
The API interface is a simple RESTFul API. It only currently supports GET as this project is only testing out a simple
read-mode access - i.e. there is no need for PUT/PATCH/DELETE against this interface. The API is serviced through the same
code as the HTML Pages, so you run the module in the same manner (see above).

You can use the [Postman](https://www.getpostman.com/) application to drive the API.

There is currently only one active route for the API:

1. GET /api/latest/:tag -> Shows a summary of the latest 10 tweets containing the hashtag ':tag' as JSON.

## Cloning
This application uses minimal dependencies.   

Feel free to use this at your will, there is no licence or restrictions associated with it over and above that of the
Dispatch.php and TwitterAPIExchange.php modules.
