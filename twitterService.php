<?php

/**
 * Here we are using the 'Dispatch.php' code from 'noodlehaus'
 */
require 'utils/Dispatch.php';

// Pull in our own helpers
require ('utils/Helpers.php');
require ('classes/TwitterHashtagSearch.php');
require 'routes/APIRoutes.php';
require 'routes/HTMLRoutes.php';

// Access tokens (keep these external so that they are not saved as part of source code and add to .gitignore)
//
// The below file is ignore in git so that we do not push our credentials up with the source code. There is a file
// called config-sample.php that shows the format for this file. This sample file should be renamed and then you should
// fill that in with your own credentials. If you do not know how to create Twitter Credentials, see:
//
//          http://docs.inboundnow.com/guide/create-twitter-application/
//
$settings = require('config.php');

// Create our Twitter access point
$twitter = new TwitterHashtagSearch( $settings );

// Run the dispatcher to listen for HTTP requests (arguments you pass here get forwarded to the route actions)
dispatch($twitter);
