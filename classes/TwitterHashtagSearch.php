<?php

require_once ( __DIR__. '/../utils/TwitterAPIExchange.php');

/**
 * Class TwitterHashtagSearch
 *
 * A simple wrapper for the Twitter API in order to abstract it away from the end uesr.
 *
 * Here we use the TwitterAPIExchange module in order that we are not re-inventing the wheel. This is a 'common' wrapper
 * for the Twitter API and removes the need to recode the oAuth / handshaking required to interact with the Twitter API
 *
 */
class TwitterHashtagSearch
{
    protected $baseURL  = "https://api.twitter.com/1.1/search/tweets.json";
    protected $method   = "GET";
    protected $query    = "?q=%%23%s&count=%d&lang=eu";
    protected $count    = 10;
    protected $results  = [];
    protected $settings = [];

    /**
     * TwitterHashtagSearch constructor.
     *
     * THe constructor accepts an array of settings to pass through to the TwitterAPIExchange object.
     *
     * @param array $settings
     */
    public function __construct( array $settings )
    {
        $this->settings = $settings;
    }

    /**
     * This function performs the actual search on the Twitter API.
     *
     * As this is a PoC, I have not added much in the way of error handling. To productionise this we would need to
     * handle appropriate exceptions and also validate the returned content from the Twitter API to check for error codes.
     *
     * @param string $tag
     * @param int $count
     * @throws Exception
     */
    public function search( string $tag, int $count = 10 )
    {
        // Build the query string
        $count = min( abs($count), 100 );
        $query = sprintf( $this->query, $tag, $count );

        // Run the query
        $twitter = new TwitterAPIExchange($this->settings);
        $this->results = json_decode(utf8_decode($twitter->setGetfield($query)->buildOauth($this->baseURL, $this->method)->performRequest()), true);

        // Would normally check for errors....
    }

    /**
     * This function formats the results into a summary and returns it as an associative array.
     *
     * The summary is an abstracted entity and uses its own key names in order to hide the implementation of the Twitter
     * API from the end user. If this were not a PoC, then we would probably encapsulate the content further behind an
     * appropriate interface object.
     *
     * @return array
     */
    public function getSummary() : array
    {
        $summary = [];
        if ( isset($this->results['statuses']) ) {
            foreach ($this->results['statuses'] as $status) {
                $entry = [];

                // Pull in the required fields (default any missing values to stop exceptions)
                $entry['id'] = $status['id_str'] ?? '-n/a-';
                $entry['created'] = $status['created_at'] ?? '-n/a-';
                $entry['text'] = trim($status['text'] ?? '-n/a-');
                $entry['truncated'] = $status['truncated'] ?? '-n/a-';

                // Pull in the hastags (default any missing values to stop exceptions)
                $entities = $status['entities'] ?? [];
                $hashtags = $entities['hashtags'] ?? [];
                $tags = [];
                foreach ($hashtags as $hashtag) {
                    $tags[] = trim($hashtag['text'] ?? '-n/a-');
                }
                $entry['tags'] = $tags;

                // Pull in the user (default any missing values to stop exceptions)
                $user = $status['user'] ?? [];
                $entry['posted_by'] = trim($user['screen_name'] ?? '-n/a-');

                // Add this to the summary as an abstracted entity
                $summary[] = $entry;
            }
        }
        return $summary;
    }

    /**
     * This function is here more from a testing / implementation viewpoint as it allows access to the underlying
     * format of the Twitter JSON in order that we can view it's strucutre in its native format.
     *
     * @return array
     */
    public function rawResults() { return $this->results; }
}
