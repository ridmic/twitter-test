<?php

// ===============================================================
// This section contains our HTML endpoints
// ===============================================================

/**
 * HOME = Welcome page
 */
route('GET', '/', function () {
    $html = phtml(__DIR__.'/../views/Welcome');
    return response($html);
});

/**
 * LATEST = Our latest tag end point through a HTML Template View
 */
route('GET', '/latest/:tag', function ($args, $twitter) {
    /**
     * @var TwitterHashtagSearch $twitter
     */

    // Get the required hastag
    $tag = $args['tag'];
    // Search for it and return the first 10
    $twitter->search( $tag, 10 );
    // pull back the summaries
    $summaries = $twitter->getSummary();
    // and pass them into our template
    $html = phtml(__DIR__.'/../views/Latest', ['summaries' => $summaries]);

    return response($html);
});

/**
 * Catch-all route
 */
route('GET', '.*', function () {
    return notFound();
});