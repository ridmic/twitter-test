<?php

// ===============================================================
// This section contains our API endpoints
// ===============================================================

/**
 * LATEST = Our latest tag end point for API calls returned as JSON
 */
route('GET', '/api/latest/:tag', function ($args, $twitter) {
    /**
     * @var TwitterHashtagSearch $twitter
     */

    $tag = $args['tag'];
    $twitter->search( $tag, 10 );

    return response(json_encode([ 'summaries' => $twitter->getSummary()], JSON_PRETTY_PRINT),
        200, ['content-type' => 'application/json']);
});
