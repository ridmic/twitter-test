<?php

/**
 * A simple wrapper to test the Twitter Service from the command line
 *
 * This is set up to just search for the latest 10 items that contain the hashtag #worldcup
 *
 * To modify the parameters, simply change the:
 *
 *      $twitter->search( 'WorldCup', 10 );
 *
 * call appropriately.
 *
 */

require_once (__DIR__. '/classes/TwitterHashtagSearch.php');

// Access tokens (keep these external so that they are not saved as part of source code and add to .gitignore)
//
// The below file is ignore in git so that we do not push our credentials up with the source code. There is a file
// called config-sample.php that shows the format for this file. This sample file should be renamed and then you should
// fill that in with your own credentials. If you do not know how to create Twitter Credentials, see:
//
//          http://docs.inboundnow.com/guide/create-twitter-application/
//
$settings = require('config.php');

// Create our Twitter Object
$twitter = new TwitterHashtagSearch( $settings );
// Run the search
$twitter->search( 'WorldCup', 10 );
// and then pull out the summaries
$summaries = $twitter->getSummary();

// Display them to the console.
foreach ($summaries as $summary )
{
    //var_dump($status);
    echo "ID: " . $summary['id'] . "\n";
    echo "  -> Created: " . $summary['created'] . "\n";
    echo "  -> Text: " . $summary['text']. "\n";
    echo "  -> Truncated: " . ($summary['truncated'] ? "Yes" : "No") . "\n";

    // Pull in the hastags
    foreach ($summary['tags'] as $hashtag) {
        echo "    -> Tags: #" . $hashtag . "\n";
    }

    // Pull in the user
    echo " Posted By: " . $summary['posted_by'] . "\n";

    echo "\n";
}